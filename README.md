# soal-shift-sisop-modul-3-C10-2022
Pembahasan soal praktikum Modul 3 Sistem Operasi 2022

**Anggota Kelompok**:

- Sarah Alissa Putri - 5025201272
- Monica Narda Davita - 5025201009
- William Zefanya Maranatha - 5025201167

---
# Daftar Isi
- [Soal 1](#soal-1)
    - [Soal 1.a](#soal-1a)
    - [Soal 1.b](#soal-1b)
    - [Soal 1.c](#soal-1c)
    - [Soal 1.d](#soal-1d)
    - [Soal 1.e](#soal-1e)
- [Soal 2](#soal-2)
    - [Soal 2.a](#soal-2a)
    - [Soal 2.b](#soal-2b)
    - [Soal 2.c](#soal-2c)
    - [Soal 2.d](#soal-2d)
    - [Soal 2.e](#soal-2e)
    - [Soal 2.f](#soal-2f)
    - [Soal 2.g](#soal-2g)
- [Soal 3](#soal-3)
    - [Soal 3.a](#soal-3a)
    - [Soal 3.b](#soal-3b)
    - [Soal 3.c](#soal-3c)
    - [Soal 3.d](#soal-3d)

---
## Soal 1
>Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

### Soal 1.a
>Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

Pertama, inisialisasi array untuk menampung thread, dalam kasus ini ada 2 thread
```ruby
pthread_t tid[2]; //download_unzip
```
Buat fungsi `download_unzip` kemudian deskripsikan link download di `*url[]`, file zip di `*fileName[]`, dan nama folder di `*folder[]`.
```ruby
void* download_unzip(void *arg){
    unsigned long i=0;
	pthread_t id=pthread_self();
	int iter, status;

    //link download
    char *url[] = {"https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download",
		           "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"};
	
    //nama file
    char *fileName[] = {"quote.zip","music.zip"};

    //folder
    char *folder[] = {"quote","music"};
```
Selanjutnya, buat thread pertama untuk membuat directory baru, mendownload file zip dari link gdrive, mengekstrak file, dan menghapus file yang ada untuk keperluan array pertama yaitu file quote.
```ruby
if(pthread_equal(id, tid[0])){
        //make dir
		if(fork() == 0){
			char *argv[] = {"mkdir", "-p", folder[0], NULL};
			execv("/usr/bin/mkdir", argv);
		}
		while (wait(&status) > 0);
	
        //download
		if(fork() == 0){
			char *argv[] = {"wget", "-q", "--no-check-certificate", url[0], "-O", fileName[0], NULL};
			execv("/usr/bin/wget", argv);
		}
		while (wait(&status) > 0);
		
        //extract
		if(fork() == 0){
			char *argv[] = {"unzip", fileName[0], "-d", "/home/sarah/quote", NULL};	
			execv("/usr/bin/unzip", argv);
		}
		while (wait(&status) > 0);
		
        //remove existing file
		if(fork() == 0){
			char *argv[] = {"rm", fileName[0], NULL};
			execv("/usr/bin/rm", argv);
		}
		while (wait(&status) > 0);
```

Lakukan hal yang sama pada thread kedua untuk keperluan file music.

```ruby
}else if(pthread_equal(id, tid[1])){
         if(fork() == 0){
			char *argv[] = {"mkdir", "-p", folder[1], NULL};
			execv("/usr/bin/mkdir", argv);
		}
		while (wait(&status) > 0);
	
		if(fork() == 0){
			char *argv[] = {"wget", "-q", "--no-check-certificate", url[1], "-O", fileName[1], NULL};		
			execv("/usr/bin/wget", argv);
		}
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"unzip", fileName[1], "-d", "/home/sarah/music", NULL};
			execv("/usr/bin/unzip", argv);
		}	
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"rm", fileName[1], NULL};	
			execv("/usr/bin/rm", argv);
		}
		while (wait(&status) > 0);
    }
    return NULL;
```

## Soal 1.b
>Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

Inisisalisasi array untuk menampung thread, dalam kasus ini menggunakan dua thread.
```ruby
pthread_t tid1[2]; //decode
```

Array character set dari base64
```ruby
char base64_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
```

Selanjutnya, buat fungsi untuk decode base64
```ruby
char *base64_decode(char *encoded)
{
    char counts = 0;
    char buffer[4];
     // alokasikan memori untuk plaintext dengan ukuran 3/4 dari encoded
    char *decoded = malloc(strlen(encoded) * 3 / 4);
    int i = 0, p = 0;

    //looping pada tiap character encoded sampai akhir (\0)
    for (i = 0; encoded[i] != '\0'; i++)
    {
        char k;
         // selama k < 64 dan indeks k pada map tidak sama dengan indeks i pada encoded, tambah nilai k
        for (k = 0; k < 64 && base64_map[k] != encoded[i]; k++)
            ;

        //simpan k ke dalam buffer selanjutnya
        buffer[counts++] = k;
        if (counts == 4)
        {
            decoded[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if (buffer[2] != 64)
                decoded[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if (buffer[3] != 64)
                decoded[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }
    decoded[p] = '\0';
    return decoded;
}
```

Buat thread untuk decode,
```ruby
void *thread_decode(void *arg)
{
    pthread_t id = pthread_self();
    char path1[10];
    char path2[12];
    char filename[10];
    // jika thread pertama maka music
    if (pthread_equal(id, tid2[0]))
    {
        strcpy(path1, "./music/");
        strcpy(path2, "./music/%s");
        strcpy(filename, "music.txt");
    }
    // jika bukan maka quote
    else
    {
        strcpy(path1, "./quote/");
        strcpy(path2, "./quote/%s");
        strcpy(filename, "quote.txt");
    }
    DIR *dir;
    struct dirent *dp;
    dir = opendir(path1);
    FILE *file, *txt;

    if (dir != NULL)
    {
        // perulangan pada tiap file di dir
        while ((dp = readdir(dir)))
        {
            char str[100] = "";
            char fullpath[1000] = "";
            // jika bukan . atau ..
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
            {
                // ambil fullpath dari path2 ditambah nama file
                sprintf(fullpath, path2, dp->d_name);
                // buka file
                file = fopen(fullpath, "r");
                // baca file encoded
                while (fgets(str, 100, file) != NULL)
                {
                    // buka file hasil
                    txt = fopen(filename, "a");
                    // masukkan hasil decode ke file hasil
                    fprintf(txt, "%s\n", base64_decode(str));
                    fclose(txt);
                }
                fclose(file);
            }
        }

        (void)closedir(dir);
    }
    else
        perror("Cannot open directory");
    return NULL;
}
```
### Soal 1.c
>Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

Buat folder baru bernama `hasil`.
```ruby
if(fork() == 0){
		char *argv[] = {"mkdir", "-p", "hasil", NULL};	
		execv("/usr/bin/mkdir", argv);
	}
	while(wait(&status) > 0);
```

```ruby
if(fork() == 0){
    char *argv[] = {"mv", "music.txt", "hasil/music.txt", NULL};
        execv("/usr/bin/mv", argv);
    }
    while(wait(&status) > 0);

    if (fork() == 0)
    {
        char *argv[] = {"mv", "quote.txt", "hasil/quote.txt", NULL};
        execv("/usr/bin/mv", argv);
    }
   while(wait(&status) > 0);
```
### Soal 1.d
>Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

```ruby
if(fork()==0){
		char *argv[] = {"zip", "-rmvqP", "mihinomenestsarah", "-r", "hasil", "hasil",NULL}; 
		execv("/usr/bin/zip", argv);
	}while(wait(&status) > 0);
	
	for(int i = 0; i < 2; i++){	
	if(fork() == 0){
		char *argv[] = {"rm", "-rf", folder[i], NULL};
		execv("/usr/bin/rm", argv);
	}
	}
```

### Soal 1.e
>Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

Karena kita perlu memasukkan file baru, maka kita harus meng-unzip terlebih dahulu folder hasil.
```ruby
void unzip(){
  pid_t child;
  child = fork();
  if(fork() == 0){
    char *unzip[5] = {"unzip", "-P","mihinomenestsarah", "hasil.zip",  NULL};
	execv ("/usr/bin/unzip",unzip);
  }else {
    printf("close\n");
	return;
  }
}
```

Inisialisasi array untuk menampung thread, pada kasus ini kita menggunakan dua thread.
```ruby
pthread_t tid2[2]; //unzip 
```
Pada thread pertama, gunakan untuk memanggil fungsi unzip yang sudah dibuat sebelumnya. Thread kedua gunakan untukk memasukkan kata `No` ke dalam file `no.txt`.
```ruby
void* notxt(void *arg){
    unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	if(pthread_equal(id,tid2[0]))
	{
		unzip();
	}
	else if(pthread_equal(id,tid2[1]))
	{	
		char *filename = "no.txt";
		FILE *fw = fopen(filename, "w");
		if (fw == NULL)
		{
			printf("Error opening the file %s", filename);
			return 0;
		}
		fprintf(fw, "NO\n");
		fclose(fw);
	}
	return NULL;
}
```
Pada fungsi main, jalankan ketiga thread yang sudah dibuat.
```ruby
int main(){
    int i=0, j=0, k=0;
    int err;
    int status;

    //folder
    char *folder[] = {"quote","music","hasil"};

    if(fork() == 0){
		char *argv[] = {"mkdir", "-p", "hasil", NULL};	
		execv("/usr/bin/mkdir", argv);
	}
	while(wait(&status) > 0);


while(i<2) // loop sejumlah thread
	{
		err=pthread_create(&(tid[i]),NULL,&download_unzip,NULL); //membuat thread
		if(err!=0){ //cek error
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

while (j < 2)
    {
        err = pthread_create(&(tid1[j]), NULL, &thread_decode, NULL);
        if(err!=0){ //cek error
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else{
			printf("\n create thread success\n");
		}
        j++;
    }
    pthread_join(tid1[0], NULL);
    pthread_join(tid1[1], NULL);

    if(fork() == 0){
    char *argv[] = {"mv", "music.txt", "hasil/music.txt", NULL};
        execv("/usr/bin/mv", argv);
    }
    while(wait(&status) > 0);

    if (fork() == 0)
    {
        char *argv[] = {"mv", "quote.txt", "hasil/quote.txt", NULL};
        execv("/usr/bin/mv", argv);
    }
   while(wait(&status) > 0);

   if(fork()==0){
		char *argv[] = {"zip", "-rmvqP", "mihinomenestsarah", "-r", "hasil", "hasil",NULL}; 
		execv("/usr/bin/zip", argv);
	}while(wait(&status) > 0);
	
	for(int i = 0; i < 2; i++){	
	if(fork() == 0){
		char *argv[] = {"rm", "-rf", folder[i], NULL};
		execv("/usr/bin/rm", argv);
	}
	}

    while (k < 2)
    {
        err = pthread_create(&(tid2[k]), NULL, &notxt, NULL);
        if(err!=0){ //cek error
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else{
			printf("\n create thread success\n");
		}
        k++;
    }
    pthread_join(tid2[0], NULL);
    pthread_join(tid2[1], NULL);
```
Masih pada fungsi main, pindahkan file `no.txt` ke dalam folder hasil kemudian unzip dengan password yang sama. Hapus file yang sudah tidak diperlukan.
```ruby
if(fork() == 0){
    char *argv[] = {"mv", "no.txt", "hasil/no.txt", NULL};
        execv("/usr/bin/mv", argv);
    }

    if(fork()==0){
        char *args[] = {"zip", "-q","hasil.zip", "-r","--password", "mihinomenestsarah", "hasil/", "no.txt", NULL}; 
        execv("/usr/bin/zip", args);
        }while(wait(&status) > 0);

    if(fork() == 0){
		char *argv[] = {"rm", "-rf", folder[i], NULL};
		execv("/usr/bin/rm", argv);
	}
	exit(0);
	return 0;
```

## Soal 2
**[Source Code soal2](https://gitlab.com/MonicaDavita/soal-shift-sisop-modul-3-c10-2022/-/tree/main/soal2)**
### Soal 2.a
**Deskripsi:**\
Suatu sistem online judge dibuat dengan sistem client-server. Kriteria pertama online judge tersebut haruslah memiliki fitur `register` dan `login`. Bila client memilih register, maka client diminta untuk memasukkan username dan password. Password harus minimal 6 karakter dan terdiri dari huruf kapital, huruf kecil, dan angka. Username tidak boleh sama ketika tiap register. Data client disimpan pada file `users.txt` dan disimpan pada direktori Server. Berikut potongan kode beserta penjelasannya:
```ruby
void *portal(void *argv){
int authorized = 0, new_socket;
    new_socket = *((int *)argv);
    free(argv);
    char server_buffer[1024], command_buffer[1024], username[100];
    if (authorized == 0)
    {
        send(new_socket, "Register or Login?\n", 100, 0);
        read(new_socket, server_buffer, 1024);
        send(new_socket, "Username: ", 100, 0);
        read(new_socket, username, 1024);

```
Potongan kode di atas menunjukkan fungsi `portal` yang merupakan perantara register dan login client. Nilai `authorized` di-assign dengan 0 terlebih dahulu yang menandakan server belum dikunjungi oleh client sehingga perlu ditampilkan pesan untuk register dan login.

Pertama, server mengirim socket berupa pesan "register atau login" lalu server membaca input user yang ada dalam socket tersebut dan disimpan pada array of char `server_buffer`. 

```ruby
if (strcmp(server_buffer, "Register") == 0)
        {
            FILE *f;
            f = fopen("users.txt", "a+");
            char password[100];
            if (unameIsValid(username, f))
            {
                send(new_socket, "Password: ", 100, 0);
                read(new_socket, password, 1024);
                if (passIsValid(password))
                {
                    printf("Username %s\n", username);
                    printf("Password %s\n", password);
                    fprintf(f, "%s:%s\n", username, password);
                    fclose(f);
                }
```
Bila client menginput "Register", maka server akan membuat atau menulis file `users.txt` dan menyimpannya pada folder `Server`. Untuk memvalidasi username, maka akan dipanggil fungsi `unameIsValid` dengan parameter string username dan pointer file `users.txt` yang menyimpan data users. Bila username valid, maka client diminta untuk menginput password lalu akan dicek validitasnya melalui fungsi 'passIsValid()'.
```ruby
int unameIsValid(char username[], FILE *fp)
{
    char buff[200];
    while (fscanf(fp, "%s", buff) != EOF)
    {
        char *tok = strtok(buff, ":");
        if (strcmp(username, tok))
        {
            return 1;
        }
    }
    return -1;
}
```
Fungsi tersebut mengembalikan nilai -1 bila username ditemukan pada file, dan 1 (`true`) bila usernamenya baru. Array of char `buff` digunakan untuk menyimpan strings pada file `users.txt`. Dan `tok[]` digunakan untuk menyimpan strins usernames user.

```ruby
int passIsValid(char password[])
{
    int upper = 0, lower = 0;
    if (strlen(password) > 6){
        for (int i = 0; i < strlen(password) + 1; i++) {
            if (isupper(password[i])) upper++;
            else if(islower(password[i])) lower++;
        }
        if(upper!=0 && lower!=0) return 1;
        else return 0; 
    }else return 0;
}
```
Variabel `upper` dan `lower` digunakan untuk memastikan apakah password sudah terdiri dari huruf kapital dan kecil. Pertama, dapat dicek terlebih dahulu panjang dari password, bila panjangnya sudah tidak memenuhi (kurang dari 6), maka password tidak valid (`return 0`). Bila valid, maka akan dilakukan pengecekan huruf kapital dan kecilnya. Bila nilai `upper` dan `lower` tidak sama dengan 0, maka dapat dipastikan password sepenuhnya valid. Bila tidak, maka password tidak valid.
Bila username dan password sudah tervalidasi, maka data user akan disimpan pada `users.txt`
```ruby
fprintf(f, "%s:%s\n", username, password);
```

Potongan kode di bawah ini adalah handler jika username dan password tidak valid:
```ruby
else
                {
                    send(new_socket, "Password is invalid", 100, 0);
                }
            }
            else
            {
                send(new_socket, "The same username has found, please login or input the different username", 100, 0);
                close(new_socket);
                exit;
            }
```
Server akan mengiri pesan seperti gambar di atas dan menutup socket sehingga client yang ingin register ulang perlu memulai dari awal lagi. 

Bila client ingin Login, maka sintaks yang akan dijalankan adalah sebagai berikut:
```ruby
}else if(strcmp(server_buffer, "Login") == 0){
            login(new_socket, username);
        }
        authorized = 1;
    }
```
Pada kondisi tersebut, akan dipanggil fungsi `login()` yang memiliki parameter socket dan username yang sudah dimasukkan. Lalu bila user sudah berhasil login, maka itu artinya client sudah terautorisasi dan dapat melakukan commands selanjutnya.
Berikut merupakan penjelasan fungsi `login()`:
```ruby
void login(int new_socket, char username[]){
    char passwordLogin[100];
            send(new_socket, "Password:", 100, 0);
            read(new_socket, passwordLogin, 1024);
            FILE *fp = fopen("/home/monica/soal-shift-sisop-modul-3-c10-2022/soal2/Server/users.txt", "a+");
            char buff[100], compare[201];
            snprintf(compare, 201, "%s:%s", username, passwordLogin);
            while (fscanf(fp, "%s", buff) != EOF){
                if (strcmp(compare, buff)==0){
                    printf("Login success!\n");
                    fclose(fp);
                    return;
                }
            }
            send(new_socket, "Failed login", 100, 0);
            fclose(fp);
}
```
Singkatnya, validasi login memiliki alur yang sama dengan validasi username pada register. 
Path yang digunakan berbeda karena file `users.txt` berada pada direktori Server.

Bila user sudah berhasil login (baik dari registrasi maupun login secara langsung) atau terautorisasi, maka pada fungsi portal akan dieksekusi sintaks berikut:
```ruby
while(authorized){
        char cwd[100];
        if(getcwd(cwd, sizeof(cwd))!=NULL) printf("You are now at %s direcory\n", cwd);

        chdir("/home/monica/");
        send(new_socket, "Login successed. Please insert one of these command\n1. add\n2. see\n3. download <problem-title>\n4. submit <problem-title> <path-file-output.txt>", 200, 0);
        read(new_socket, command_buffer, 1024);
        if(strcmp(command_buffer, "add") == 0) add(new_socket, username);
        else if(strcmp(command_buffer, "see") == 0) see(new_socket, username);
        else if(command_buffer[0] == 'd') downloadProblem(new_socket, command_buffer);
        else if(command_buffer[0] == 's') submit(new_socket, command_buffer);
        else close(new_socket);
    }
} //end of portal()
```
Karena commands yang akan dijalankan melibatkan direktori user, maka kita perlu mengubah direktori awal (`cwd`) ke direktori user. Baris-baris selanjutnya menampilkan pilihan yang dapat dilakukan user yaitu add, see, download, dan submit.

**Kendala**:
- Kesulitan dalam validasi password dan penggunaan thread pada kasus ini.

### Soal 2.b
**Deskripsi:**\
Menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t.

```ruby
void add(int fd, char username[]){
    FILE *ftsv = fopen("problems.tsv", "a+");
    char title[100], desc[100], input[100], output[100], ch;
    send(fd, "Title: ", 100, 0);
    read(fd, title, 1024);
    if(problemIsNotValid(title)){
        send(fd, "re_inp", 200,0);
        send(fd, "Title exists. Input another title: ", 150,0);
        sleep(1);
        add(fd, username);
    }
```
File `problem.tsv` akan digenerate saat user memilih command "add". Selain itu, client diminta memasukkan judul problem karena user ingin menambah problem baru (berkaitan dengan soal berikutnya). Pertama akan diperiksa terlebih dahulu apakah judul problem ada yang sama atau tidak dengan memanggil fungsi `problemIsValid()`. 
```ruby
int problemIsNotValid(char title[]){
    FILE *ftsv = fopen("problems.tsv", "r");
    char buff[200];
    while(fgets(buff, 200, ftsv)){
        buff[strcspn(buff, "\n")] = 0;
        char *tok = strtok(buff, "\t");
        if(strcmp(title, tok) == 0) return 1;
    }
    fclose(ftsv);
    return 0;
}
```
Fungsi tersebut akan membuka file `problems.tsv` dan hanya membaca isinya. Selama `buff`, string yang menyimpan kalimat pada file, tidak kosong maka akan dilakukan komparasi yang kosepnya sama seperti validasi username. Untuk pembandingannya sendiri akan menggunakan bantuan variabel `tok[]` yang menyimpan string judul pada file. Bila ditemukan judul yang sama maka problem tidak valid (`return 1`). Bila tidak sama, maka problem valid (`return 0`).

**Kendala:**
- Memvalidasi judul pada file problem.tsv

### Soal 2.c
**Deskripsi:**\
Memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem.
```ruby
send(fd, "Input the description filepath: ", 100, 0);
    read(fd, desc, 1024);
    send(fd, "Input the input filepath: ", 100, 0);
    read(fd, input, 1024);
    send(fd, "Input the output filepath: ", 100, 0);
    read(fd, output, 1024);

    fprintf(ftsv, "%s\t%s\n", title, username);
    fclose(ftsv);
    mkdir(title, 0777);
    chdir(title);
    copy(desc, "description.txt");
    copy(input, "input.txt");
    copy(output, "output.txt");
}
```
Setelah `problem.tsv` sudah terbentuk maka client akan diminta memasukkan judul terlebih dahulu (seperti penjelasan sebelumnya). Setelah itu baru client memasukkan filepath deskripsi, input, dan output yang dalam format `.txt`. Selanjutnya, judul problem dan username client akan disimpan pada `problem.tsv` menggunakan `fprintf()` setelah itu file pointer akan ditutup. Selanjutnya akan dibuat direktori sesuai judul problem dan posisi server sekarang berada di folder tersebut. 

Akan dipanggil fungsi `copy()` yang gunanya adalah untuk menyalin isi deskripsi, input, dan output pada folder user ke folder yang sesuai yang diperi nama `description.txt`, `input.txt`, dan `output.txt`.  
```ruby
void copy(char src[], char dest[]){
    FILE *f1 = fopen(src, "a+");
    FILE *f2 = fopen(dest, "a+");
    char ch;
    ch = fgetc(f1);
    while(ch != EOF){
        fputc(ch, f2);
        ch = fgetc(f1);
    }
    fclose(f1);
    fclose(f2);
}
```
Pada umumnya, fungsi tersebut akan membuka file user dan mengambil karakternya satu per satu dan `put` pada file destinasi.

**Kendala:**
- Sempat mengalami kendala tidak bisa melakukan copy (file pada direktori judul kosong walaupun file user ada isinya).


### Soal 2.d
**Deskripsi:**\
Memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut).
```ruby
void see(int fd, char username[]){
    char cwd[200];
    if(getcwd(cwd, sizeof(cwd))!=NULL){
        printf("You are now at %s directory\n", cwd);
    }

    FILE *ftsv = fopen("problems.tsv", "a+");
    char buff[1001], c;
    c = fgetc(ftsv);
    while(c != EOF){
        if(c == '\t'){
            strcat(buff, " by ");
        }
        else{
            strncat(buff, &c, 1);
        }
        c = fgetc(ftsv);
    }
    send(fd, buff, 1001, 0);
}
```

### Soal 2.e
**Deskripsi:**\
Command ‘download <judul-problem>’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu <judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama <judul-problem> di client.
```ruby
void downloadProblem(int sock, char pathTitle[]){
    char buff[100], filepath1[100] = "/home/monica/soal-shift-sisop-modul-3-c10-2022/soal2/Client/", filepath2[100]="/home/monica/soal-shift-sisop-modul-3-c10-2022/soal2/Client/";
    char dir[100] = "/home/monica/soal-shift-sisop-modul-3-c10-2022/soal2/Client/";

    for(int i=9; i<strlen(pathTitle)+1; i++){
        buff[i-9] = pathTitle[i];
    }
    chdir(buff);
    strcat(dir, buff);
    strcat(filepath1, buff);
    strcat(filepath1, "/description.txt");
    strcat(filepath2, buff);
    strcat(filepath2, "/input.txt");
    mkdir(dir, 0777);
    copy("decription.txt", filepath1);
    copy("input.txt", filepath2);
}
```
Index dimulai dari 9 karena command yang diberikan client adalah "download " yang sebanyak 9 karakter, sehingga untuk menerima judul yang diminta perlu dimulai dari index ke 9. Argumen kedua pada `mkdir()` mengindikasikan autoritas dalam pembuatan direktori. Dilakukan copy hanya pada file description dan input saja karena tidak mungkin user mendownload kunci jawabannya.

### Soal 2.f
**Deskripsi:**\
Command ‘submit <judul-problem> <path-file-output.txt>’. Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.
```bash
void submit(int sock, char pathTitle[]){
    char cwd[200];
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        printf("You are now at %s directory\n", cwd);
    }
    printf("%s\n", pathTitle);
    int pf = 0, len = 0;
    char buff[100], filepath1[100], filepath2[100], ch, ch_sub;
    for (int i=7; i < strlen(pathTitle); i++)
    {
        buff[i-7] = pathTitle[i];
    }
    for (int i=0; i < strlen(buff); i++)
    {
        if (buff[i] == ' ')
        {
            filepath1[i] = '\0';
            pf = 1;
        }
        else if (!pf)
        {
            filepath1[i] = buff[i];
        }
        else
        {
            filepath2[len] = buff[i];
            len++;
        }
    }
    printf("%s\n%s\n", filepath1, filepath2);
    chdir(filepath1);
    FILE *out = fopen("output.txt", "a+"), *sub = fopen(filepath2, "a+");
    ch = fgetc(out);
    ch_sub = fgetc(sub);
    while (ch != EOF)
    {
        printf("%s\n", filepath1);
        if (ch != ch_sub)
        {
            send(sock, "WA", 5, 0);
            return;
        }
        ch_sub = fgetc(sub);
        ch = fgetc(out);
    }
    fclose(out);
    fclose(sub);
    send(sock, "AC", 5, 0);
}
```

### Soal 2.g
**Deskripsi:**\
Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.
```ruby
pthread_t thr;
            int *ptr_client = malloc(sizeof(int));
            *ptr_client = new_socket;
            pthread_create(&thr, NULL, &portal, ptr_client);
            s_count++;
```
Ini merupakan potongan sintaks pada fungsi `main()` setelah socket dibuat.

### Inisiasi Socket
```bash
int main(int argc, char const *argv[])
{
    struct sockaddr_in address;
    int new_socket, addrlen = sizeof(address), s_count = 0;
    while (1)
    {
        if (s_count==0)
        {
            int server_fd = createServer(address, addrlen);
            if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
            {
                perror("accept");
                exit(EXIT_FAILURE);
            }
            //printf("success\n");
            pthread_t thr;
            int *ptr_client = malloc(sizeof(int));
            *ptr_client = new_socket;
            pthread_create(&thr, NULL, &portal, ptr_client);
            s_count++;
        }
    }
    return 0;
}
```

Fungsi createServer
```bash
int createServer(struct sockaddr_in address, int addrlen){
    int server_fd, op;
    op = 1;
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
            {
                perror("socket has failed");
                exit(EXIT_FAILURE);
            }

            if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &op, sizeof(op)))
            {
                perror("setsockop");
                exit(EXIT_FAILURE);
            }

            address.sin_family = AF_INET;
            address.sin_addr.s_addr = INADDR_ANY;
            address.sin_port = htons(PORT);

            if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
            {
                perror("Error in binding");
                exit(EXIT_FAILURE);
            }

            if(listen(server_fd, 3)<0){
                perror("listen");
                exit(EXIT_FAILURE);
            }
            return server_fd;
}
```

### Kode pada Client
```ruby
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#define PORT 8080

int createClient(struct sockaddr_in server_addr);

int main(){
    struct sockaddr_in server_addr;
    int valread, sock;
    sock = createClient(server_addr);

    while(1){
        fflush(stdin);
        char buff[100] = {0}, server_buff[1024] = {0};
        read(sock, server_buff, 1024);

        if(strcmp(server_buff, "multi-input")==0){
            printf("%s\n", server_buff);
            memset(server_buff, 0, strlen(server_buff));
            read(sock, server_buff, 1024);
            printf("%s\n", server_buff);
            memset(server_buff, 0, strlen(server_buff));
            read(sock, server_buff, 1024);
        }
        printf("%s\n", server_buff);
        fgets(buff, 200, stdin);
        buff[strcspn(buff, "\n")] = 0;

        send(sock, buff, 100, 0);
    }
    return 0;
}

int createClient(struct sockaddr_in server_addr){
    int sock;
    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        printf("\nFailed to create socket\n");
        return -1;
    }
    memset(&server_addr, '0', sizeof(server_addr));

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);

    if(inet_pton(AF_INET, "127.0.0.1", &server_addr.sin_addr) <= 0){
        printf("\nInvalid address\n");
        return -1;
    }
    if(connect(sock, (struct sockaddr *)&server_addr, sizeof(server_addr))<0){
        printf("\nFail to connect\n");
        return -1;
    }
    return sock;
}
```

## Bukti-Bukti
- Bukti registrasi berhasil
![registerSuccess](https://gitlab.com/MonicaDavita/foto-foto/-/blob/main/SS3/1_Berhasil_registrasi.jpg)

- Bukti registrasi gagal
![registrasiFailed](https://gitlab.com/MonicaDavita/foto-foto/-/blob/main/SS3/2_Registrasi_gagal__password_invalid.jpg)

- Bukti login gagal
![loginFailed](https://gitlab.com/MonicaDavita/foto-foto/-/blob/main/SS3/3_Login_gagal.jpg)

- Bukti problems.tsv terbentuk
![problemsTsv](https://gitlab.com/MonicaDavita/foto-foto/-/blob/main/SS3/4_Problems_tsv_terbentuk.jpg)

- Bukti fungsi add berjalan dengan baik
![addFunction](https://gitlab.com/MonicaDavita/foto-foto/-/blob/main/SS3/5_Fungsi_add_berhasil.jpg)

- Bukti fungsi add dapat membentuk problem yang sesuai
![addSuccess](https://gitlab.com/MonicaDavita/foto-foto/-/blob/main/SS3/6_Fungsi_add_berhasil.jpg)

- Bukti fungsi see berjalan dengan benar.
![seeFunction](https://gitlab.com/MonicaDavita/foto-foto/-/blob/main/SS3/7_Bukti_see.jpg)

- Bukti fungsi download berhasil.
![downloadFunction](https://gitlab.com/MonicaDavita/foto-foto/-/blob/main/SS3/8_berhasil_download.jpg)

- Bukti submit (AC)
![submitAC](https://gitlab.com/MonicaDavita/foto-foto/-/blob/main/SS3/9_Bukti_AC.jpg)

- Bukti submit (WA)
![submitWA](https://gitlab.com/MonicaDavita/foto-foto/-/blob/main/SS3/10_Bukti_WA.jpg)
## No. 3

Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.
Contoh jika program pengkategorian dijalankan

### Soal 3.A
Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif

Extract File

```
// extract zip file
void extract(char *value) {
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer,'\0',sizeof(buffer));

    file = popen(value ,"r");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n",buffer);
        pclose(file);
    }
}
```
Pada fungsi `extract` akan mengekstract file dengan `popen` dimana value nantinya akan ada command unzip pada path file dari file hartakarun.zip


```
// sorting file by category
void sort_file(char *from){
    struct_path s_path;
    struct dirent *dp;
    DIR *dir;

    flag = 1;
    int index = 0;
    strcpy(s_path.cwd, "/home/wahid/shift3/hartakarun");

    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move_move, (void *)&s_path);
            sleep(1);
            index++;
        }
    }

    if(!flag){
        printf("Maaf kawan, lupakan saja :)\n");
    }else {
        printf("Kamu bisa lanjut, tapi jangan berharap lebih :)\n");
    }
}
```

Dengan menggunakan library `dirent`, program akan membaca apa saja yang ada di directory hingga NULL. Lalu akan dijalankan thread untuk fungsi `move_move` yang akan memindahkan file menuju folder yang sesuai formatnya

```
// move
void *move_move(void *s_path){
    struct_path s_paths = *(struct_path*) s_path;
    move_file(s_paths.from, s_paths.cwd);
    pthread_exit(0);
}

// move file
void move_file(char *p_from, char *p_cwd){
    char file_name[300], file_ext[30], temp[300];
    char *buffer;

    strcpy(temp, p_from);
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }

    convert_to_lower(file_ext);

    // new dest
    strcat(p_cwd, "/");
    strcat(p_cwd, file_ext);
    strcat(p_cwd, "/");
    strcat(p_cwd, file_name);

    // information
    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    // create dir and move file
    create_dir(file_ext);
    rename(p_from, p_cwd);
}
```
Fungsi `move_move` akan memanggil fungsi `move_file` yang akan memindahkan file sesuai dengan format filenya. Pertama fungsi akan looping buffer hingga akhir dari "/" yang menandakan nama file. Nama file ditampung dan dikategorikan dengan formatnya. Folder dengan format file akan dibuat dengan fungsi `create_dir` dan file akan di pindahkan dengan fungsi `rename` yang akan memindahkan file lama ke file baru berdasarkan path yang sudah di buat di `p_cwd`

### Soal 3.B
Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.

```
while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }
```
Untuk hidden file penamaannya diawali dengan dot maka akan dicek apakah char index pertamanya untuk file hidden. untuk file unknown akan tersortir setelah else if karena buffer menampung strtok file name setelah "." yang menandakan format file.


### Soal 3.C
Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.

```
while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move_move, (void *)&s_path);
            sleep(1);
            index++;
        }
```
Di fungsi `sort_file` terdapat while loop yang mengiterasi file dalam directory hartakarun. Pada setiap iterasi akan dibuat 1 thread untuk yang memanggil fungsi `move_move` untuk memindahkan file.

### Soal 3.D
Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.

```
zip("zip -o -r /home/wahid/shift3/Client/hartakarun.zip ../hartakarun");

void zip(char *ziping){
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer, '\0', sizeof(buffer));

    //ziping
    file = popen(ziping, "w");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n", buffer);
        pclose(file);
    }
```

Pada program client, fungsi `zip()` akan menerima parameter barupa command yang akan dieksekusi dalam terminal. Pada fungsi main fungsi zip akan menerima parameter command zip pada file path hartakarun. Command ini selanjutnya akan dieksekusi dengan popen yang ada dalam fungsi `zip`.

