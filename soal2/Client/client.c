#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#define PORT 8080

int createClient(struct sockaddr_in server_addr);

int main(){
    struct sockaddr_in server_addr;
    int valread, sock;
    sock = createClient(server_addr);

    while(1){
        fflush(stdin);
        char buff[100] = {0}, server_buff[1024] = {0};
        read(sock, server_buff, 1024);

        if(strcmp(server_buff, "multi-input")==0){
            printf("%s\n", server_buff);
            memset(server_buff, 0, strlen(server_buff));
            read(sock, server_buff, 1024);
            printf("%s\n", server_buff);
            memset(server_buff, 0, strlen(server_buff));
            read(sock, server_buff, 1024);
        }
        printf("%s\n", server_buff);
        fgets(buff, 200, stdin);
        buff[strcspn(buff, "\n")] = 0;

        send(sock, buff, 100, 0);
    }
    return 0;
}

int createClient(struct sockaddr_in server_addr){
    int sock;
    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        printf("\nFailed to create socket\n");
        return -1;
    }
    memset(&server_addr, '0', sizeof(server_addr));

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);

    if(inet_pton(AF_INET, "127.0.0.1", &server_addr.sin_addr) <= 0){
        printf("\nInvalid address\n");
        return -1;
    }
    if(connect(sock, (struct sockaddr *)&server_addr, sizeof(server_addr))<0){
        printf("\nFail to connect\n");
        return -1;
    }
    return sock;
}