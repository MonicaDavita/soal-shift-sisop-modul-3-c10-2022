#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <ctype.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <netinet/in.h>
#define PORT 8080

int unameIsValid(char username[], FILE *fp)
{
    char buff[200];
    while (fscanf(fp, "%s", buff) != EOF)
    {
        char *tok = strtok(buff, ":");
        if (strcmp(username, tok))
        {
            return 1;
        }
    }
    return -1;
}

int passIsValid(char password[])
{
    int upper = 0, lower = 0;
    if (strlen(password) > 6){
        for (int i = 0; i < strlen(password) + 1; i++) {
            if (isupper(password[i])) upper++;
            else if(islower(password[i])) lower++;
        }
        if(upper!=0 && lower!=0) return 1;
        else return 0; 
    }else return 0;
}


void see(int fd, char username[]){
    char cwd[200];
    if(getcwd(cwd, sizeof(cwd))!=NULL){
        printf("You are now at %s directory\n", cwd);
    }

    FILE *ftsv = fopen("problems.tsv", "a+");
    char buff[1001], c;
    c = fgetc(ftsv);
    while(c != EOF){
        if(c == '\t'){
            strcat(buff, " by ");
        }
        else{
            strncat(buff, &c, 1);
        }
        c = fgetc(ftsv);
    }
    send(fd, buff, 1001, 0);
}

void copy(char src[], char dest[]){
    FILE *f1 = fopen(src, "a+");
    FILE *f2 = fopen(dest, "a+");
    char ch;
    ch = fgetc(f1);
    while(ch != EOF){
        fputc(ch, f2);
        ch = fgetc(f1);
    }
    fclose(f1);
    fclose(f2);
}

int problemIsValid(char title[]){
    FILE *ftsv = fopen("problems.tsv", "r");
    char buff[200];
    while(fgets(buff, 200, ftsv)){
        buff[strcspn(buff, "\n")] = 0;
        char *tok = strtok(buff, "\t");
        if(strcmp(title, tok) == 0) return 1;
    }
    fclose(ftsv);
    return 0;
}

void add(int fd, char username[]){
    FILE *ftsv = fopen("problems.tsv", "a+");
    char title[100], desc[100], input[100], output[100], ch;
    send(fd, "Title: ", 100, 0);
    read(fd, title, 1024);
    if(problemIsValid(title)){
        send(fd, "re_inp", 200,0);
        send(fd, "Title exists. Input another title: ", 150,0);
        sleep(1);
        add(fd, username);
    }
    send(fd, "Input the description filepath: ", 100, 0);
    read(fd, desc, 1024);
    send(fd, "Input the input filepath: ", 100, 0);
    read(fd, input, 1024);
    send(fd, "Input the output filepath: ", 100, 0);
    read(fd, output, 1024);

    fprintf(ftsv, "%s\t%s\n", title, username);
    fclose(ftsv);
    mkdir(title, 0777);
    chdir(title);
    copy(desc, "description.txt");
    copy(input, "input.txt");
    copy(output, "output.txt");
}

void downloadProblem(int sock, char pathTitle[]){
    char buff[100], filepath1[100] = "/home/monica/soal-shift-sisop-modul-3-c10-2022/soal2/Client/", filepath2[100]="/home/monica/soal-shift-sisop-modul-3-c10-2022/soal2/Client/";
    char dir[100] = "/home/monica/soal-shift-sisop-modul-3-c10-2022/soal2/Client/";

    for(int i=9; i<strlen(pathTitle)+1; i++){
        buff[i-9] = pathTitle[i];
    }
    chdir(buff);
    strcat(dir, buff);
    strcat(filepath1, buff);
    strcat(filepath1, "/description.txt");
    strcat(filepath2, buff);
    strcat(filepath2, "/input.txt");
    mkdir(dir, 0777);
    copy("decription.txt", filepath1);
    copy("input.txt", filepath2);
}

void submit(int sock, char pathTitle[]){
    char cwd[200];
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        printf("You are now at %s directory\n", cwd);
    }
    printf("%s\n", pathTitle);
    int pf = 0, len = 0;
    char buff[100], filepath1[100], filepath2[100], ch, ch_sub;
    for (int i=7; i < strlen(pathTitle); i++)
    {
        buff[i-7] = pathTitle[i];
    }
    for (int i=0; i < strlen(buff); i++)
    {
        if (buff[i] == ' ')
        {
            filepath1[i] = '\0';
            pf = 1;
        }
        else if (!pf)
        {
            filepath1[i] = buff[i];
        }
        else
        {
            filepath2[len] = buff[i];
            len++;
        }
    }
    printf("%s\n%s\n", filepath1, filepath2);
    chdir(filepath1);
    FILE *out = fopen("output.txt", "a+"), *sub = fopen(filepath2, "a+");
    ch = fgetc(out);
    ch_sub = fgetc(sub);
    while (ch != EOF)
    {
        printf("%s\n", filepath1);
        if (ch != ch_sub)
        {
            send(sock, "WA", 5, 0);
            return;
        }
        ch_sub = fgetc(sub);
        ch = fgetc(out);
    }
    fclose(out);
    fclose(sub);
    send(sock, "AC", 5, 0);
}

void login(int new_socket, char username[]){
    char passwordLogin[100];
            send(new_socket, "Password:", 100, 0);
            read(new_socket, passwordLogin, 1024);
            FILE *fp = fopen("/home/monica/soal-shift-sisop-modul-3-c10-2022/soal2/Server/users.txt", "a+");
            char buff[100], compare[201];
            snprintf(compare, 201, "%s:%s", username, passwordLogin);
            while (fscanf(fp, "%s", buff) != EOF){
                if (strcmp(compare, buff)==0){
                    printf("Login success!\n");
                    fclose(fp);
                    return;
                }
            }
            send(new_socket, "Failed login", 100, 0);
            fclose(fp);
}

void *portal(void *argv)
{
    int authorized = 0, new_socket;
    new_socket = *((int *)argv);
    free(argv);
    char server_buffer[1024], command_buffer[1024], username[100];
    if (authorized == 0)
    {
        send(new_socket, "Register or Login?\n", 100, 0);
        read(new_socket, server_buffer, 1024);
        send(new_socket, "Username: ", 100, 0);
        read(new_socket, username, 1024);

        if (strcmp(server_buffer, "Register") == 0)
        {
            FILE *f;
            f = fopen("users.txt", "a+");
            char password[100];
            if (unameIsValid(username, f))
            {
                send(new_socket, "Password: ", 100, 0);
                read(new_socket, password, 1024);
                if (passIsValid(password))
                {
                    printf("Username %s\n", username);
                    printf("Password %s\n", password);
                    fprintf(f, "%s:%s\n", username, password);
                    fclose(f);
                }
                else
                {
                    send(new_socket, "Password is invalid", 100, 0);
                }
            }
            else
            {
                send(new_socket, "The same username has found, please login or input the different username", 100, 0);
                close(new_socket);
                exit;
            }
        }else if(strcmp(server_buffer, "Login") == 0){
            login(new_socket, username);
        }
        authorized = 1;
    }

    while(authorized){
        char cwd[100];
        if(getcwd(cwd, sizeof(cwd))!=NULL) printf("You are now at %s direcory\n", cwd);

        chdir("/home/monica/");
        send(new_socket, "Login successed. Please insert one of these command\n1. add\n2. see\n3. download <problem-title>\n4. submit <problem-title> <path-file-output.txt>", 200, 0);
        read(new_socket, command_buffer, 1024);
        if(strcmp(command_buffer, "add") == 0) add(new_socket, username);
        else if(strcmp(command_buffer, "see") == 0) see(new_socket, username);
        else if(command_buffer[0] == 'd') downloadProblem(new_socket, command_buffer);
        else if(command_buffer[0] == 's') submit(new_socket, command_buffer);
        else close(new_socket);
    }
}
int createServer(struct sockaddr_in address, int addrlen){
    int server_fd, op;
    op = 1;
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
            {
                perror("socket has failed");
                exit(EXIT_FAILURE);
            }

            if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &op, sizeof(op)))
            {
                perror("setsockop");
                exit(EXIT_FAILURE);
            }

            address.sin_family = AF_INET;
            address.sin_addr.s_addr = INADDR_ANY;
            address.sin_port = htons(PORT);

            if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
            {
                perror("Error in binding");
                exit(EXIT_FAILURE);
            }

            if(listen(server_fd, 3)<0){
                perror("listen");
                exit(EXIT_FAILURE);
            }
            return server_fd;
}

int main(int argc, char const *argv[])
{
    struct sockaddr_in address;
    int new_socket, addrlen = sizeof(address), s_count = 0;
    while (1)
    {
        if (s_count==0)
        {
            int server_fd = createServer(address, addrlen);
            if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
            {
                perror("accept");
                exit(EXIT_FAILURE);
            }
            //printf("success\n");
            pthread_t thr;
            int *ptr_client = malloc(sizeof(int));
            *ptr_client = new_socket;
            pthread_create(&thr, NULL, &portal, ptr_client);
            s_count++;
        }
    }
    return 0;
}